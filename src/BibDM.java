import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Set;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        Integer elemmin = null;
        for(Integer i : liste){
          if(elemmin == null){
            elemmin = i;
          }
          else{
            if(i.compareTo(elemmin) < 0){
              elemmin = i;
            }
          }
        }
        return elemmin;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        boolean res = true;
        for(int i = 0; i<liste.size(); i++){
          if(liste.get(i).compareTo(valeur)<=0){
            res = false;
          }
        }

        return res;
    }

    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){

        List<T> res = new ArrayList<>();
        boolean sortir = false;
        int j = 0;

        for(int i = 0; i<liste1.size(); i++){
          sortir = false;
          j=0;
          while(j < liste2.size() && !sortir){
            if(liste1.get(i).compareTo(liste2.get(j)) == 0){
              if(!res.contains(liste1.get(i))){
                res.add(liste1.get(i));
              }
              sortir = true;
            }
            //Comme la liste est triée, si on est plus grand qu'un élément de la liste, c'est forcément que l'élement ne sera pas présent
            //dans les deux listes : on peut donc sortir de la boucle
            if(liste2.get(j).compareTo(liste1.get(i)) > 0){
              sortir = true;
            }
            j++;
          }
        }
      return res;
      }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> res = new ArrayList<>();
        //Test du mot vide
        if(texte.length() == 0){
          return res;
        }
        String motcourant = "";
        for(int i = 0; i < texte.length(); i++){
          Character cara = texte.charAt(i);
          String car = cara.toString();
          //Si jamais le caractère est = ce n'est plus un mot
          if(car.compareTo(" ") == 0){
            //Si jamais le mot actuel n'est pas vide = si on a terminé un mot
            if(motcourant.length() > 0){
              //On l'ajoute dans la liste de mots, puis on réinitialise le mot courant
              res.add(motcourant);
              motcourant = "";
            }
          }
          //Sinon si le caractère est une lettre, on l'ajoute au mot courant
          else{
            motcourant = motcourant + car;
          }
        }
        //on ajoute le dernier mot
        if(motcourant != ""){
          res.add(motcourant);
        }
      return res;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){

        String motcourant = "";
        Map<String,Integer> dicomots = new HashMap<>();

        //Tout d'abord on crée le dictionnaire des fréquences
        for(int i =0; i<texte.length(); i++){
            if(texte.charAt(i) == ' '){
              if(motcourant != ""){
                if(dicomots.containsKey(motcourant)){
                  dicomots.put(motcourant,dicomots.get(motcourant)+1);
                }
                else{
                  dicomots.put(motcourant,1);
                }
              }
              motcourant = "";
            }
            else{
              char car = texte.charAt(i);
              motcourant = motcourant + car;
            }
        }

        if(dicomots.containsKey(motcourant)){
          dicomots.put(motcourant,dicomots.get(motcourant)+1);
        }
        else{
          if(motcourant != ""){
            dicomots.put(motcourant,1);
            }
          }
        System.out.println(dicomots);

        //A partir du dictionnaire des fréquences, on regarde quel mot est le plus fréquent
        String motMax = null;
        Integer valMax = 0;
        Set<String> enscle = dicomots.keySet();
        for(String mot : enscle){
          if(motMax == null){
            motMax = mot;
            valMax = dicomots.get(mot);
          }
          else{
            if(valMax == dicomots.get(mot)){
              //On réalise le test pour prendre le premier mot dans l'ordre alphabétique si les deux sont aussi fréquents
              if(motMax.compareTo(mot) > 0){
                motMax = mot;
              }
            }
            else if(valMax < dicomots.get(mot)){
              motMax = mot;
              valMax = dicomots.get(mot);
            }
          }
        }

        return motMax;
    }

    //Fonctions réutilisées dans bienParenthesee
    public static boolean isParentheseGauche(String chaine){
      for(int i =0; i<chaine.length(); i++){
        if(chaine.charAt(i) == '('){
          return true;
        }
      }
      return false;
    }

    public static boolean isCrochetGauche(String chaine){
      for(int i =0; i<chaine.length(); i++){
        if(chaine.charAt(i) == '['){
          return true;
        }
      }
      return false;
    }


    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        boolean res = true;
        String chainepar = "";
        for(int i =0; i<chaine.length(); i++){
          //Le principe pour cette fonction est qu'on ajoute dans une liste toutes les parenthèses gauches.
          //Lorsqu'on tombe sur une parenthèse droite, on l'assemble avec la dernière parenthèse gauche de la liste.
          //Ainsi, si on ne peut pas assembler une parenthèse droite, c'est que la liste est mal parenthésée
          if(chaine.charAt(i) == ')'){
            if(isParentheseGauche(chainepar)){
              Integer indice = chainepar.lastIndexOf('(');
              String mot = "";
              for(int j = 0; j<indice;j++ ){
                mot = mot + chainepar.charAt(j);
                }
              chainepar = mot;
            }
            else{
              return false;
            }
          }
          if(chaine.charAt(i) == '('){
            chainepar = chainepar + chaine.charAt(i);
          }
        }
        if(chainepar.length() != 0){
          res = false;
        }
      return res;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      //Même principe que pour les parenthèses, cette fois ci on élimine les crochets gauches et droits ensembles,
      //Et les parenthèses gauches et droites ensemble.
      boolean res = true;
      String chainepar = "";
      for(int i =0; i<chaine.length(); i++){
        if(chaine.charAt(i) == ')'){
          if(isParentheseGauche(chainepar)){
            Integer indice = chainepar.lastIndexOf('(');
            String mot = "";
            for(int j = 0; j<indice;j++ ){
              mot = mot + chainepar.charAt(j);
              }
            chainepar = mot;
          }
          else{
            return false;
          }
        }
        else if(chaine.charAt(i) == '(' || chaine.charAt(i) == '['){
          chainepar = chainepar + chaine.charAt(i);
        }
        else if(chaine.charAt(i) == ']'){
          if(isCrochetGauche(chainepar)){
            Integer indice = chainepar.lastIndexOf('[');
            String mot = "";
            for(int j = 0; j<indice;j++ ){
              mot = mot + chainepar.charAt(j);
              }
            chainepar = mot;
          }
          else{
            return false;
          }
        }
      }
      if(chainepar.length() != 0){
        res = false;
      }
    return res;

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      boolean res = false;
      int idebut = 0;
      int ifin = liste.size()-1;
      int indice = 0;
      while(!res && (idebut < ifin)){
        if(liste.get(indice).compareTo(valeur) > 0){
          ifin = indice -1 ;
        }
        else if(liste.get(indice).compareTo(valeur) < 0){
          idebut = indice + 1;
        }
        indice = (idebut + ifin) / 2;
        if(liste.get(indice).compareTo(valeur) == 0){
          res = true;
        }
      }
      return res;
    }

}
